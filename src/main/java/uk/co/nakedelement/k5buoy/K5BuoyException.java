package uk.co.nakedelement.k5buoy;

public class K5BuoyException extends RuntimeException
{
	private static final long serialVersionUID = -5508657641878597995L;

	public K5BuoyException(Throwable arg0)
	{
		super(arg0.getMessage(), arg0);
	}
}
