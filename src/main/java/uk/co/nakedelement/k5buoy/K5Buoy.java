package uk.co.nakedelement.k5buoy;

import java.io.StringWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;

public class K5Buoy
{
	private static final Logger LOGGER = Logger.getLogger(K5Buoy.class); 
	
	public static final String DEFAULT_URL 	= "http://www.ndbc.noaa.gov/data/realtime2/64045.txt";
	
	private static final int THIRD_LINE 	= 2;
	
	private final SimpleDateFormat SDF = new SimpleDateFormat("yyyy-MM-dd hh:mm");
	
	private final URL url;
	
	public K5Buoy()
	{
		this(DEFAULT_URL);
	}
	
	public K5Buoy(String url)
	{
		this.url = toUrl(url);
	}
	
	public Results get()
	{
		final String[] lines = lines();
		final String[] keys = splitLine(lines[0]);	
		final String[] units = splitLine(lines[1]);
		
		final Map<Date, Map<String, Metric>> results = new HashMap<>();
		
		for(int lineItr = THIRD_LINE; lineItr < lines.length; ++lineItr)
		{
			final String[] tokens = splitLine(lines[lineItr]);
			
			final Map<String, Metric> metric = new HashMap<>();
			for(int metricItr = 0; metricItr < tokens.length; ++metricItr)
			{
				final String key = keys[metricItr];
				metric.put( key, new Metric().setKey(key).setValue(tokens[metricItr]).setUnit(units[metricItr]) );
			}
			
			results.put(buildDate(tokens), metric);
		}
		
		return new Results(results);
	}
	
	private Date buildDate(String[] tokens)
	{
		try
		{
			final StringBuilder builder = new StringBuilder(tokens[0]);
			builder.append("-");
			builder.append(tokens[1]);
			builder.append("-");
			builder.append(tokens[2]);
			builder.append(" ");
			builder.append(tokens[3]);
			builder.append(":");
			builder.append(tokens[4]);			
			return SDF.parse(builder.toString());
		}
		catch(Exception e)
		{
			throw new K5BuoyException(e);
		}
	}
	
	private static String[] splitLine(String line)
	{
		return line.replace("#","").split("\\s+");
	}
	
	private String[] lines()
	{
		final String file = getFile(url);
		LOGGER.debug(new StringBuilder("\n").append(file).toString());
		return file.split("\n");
	}
	
	private static String getFile(URL url) 
	{
		try
		{
		    final HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		    conn.setRequestMethod("GET");
		     
		    final StringWriter writer = new StringWriter();
		    IOUtils.copy(conn.getInputStream(), writer, StandardCharsets.UTF_8);
		    return writer.toString();
		}
		catch(Exception e)
		{
			throw new K5BuoyException(e);
		}
	}
	
	private static URL toUrl(String url)
	{
		LOGGER.debug(new StringBuilder("URL: ").append(url).toString());

		try
		{
			return new URL(url);
		}
		catch (MalformedURLException e)
		{
			throw new K5BuoyException(e);
		}
	}
	
	

}
