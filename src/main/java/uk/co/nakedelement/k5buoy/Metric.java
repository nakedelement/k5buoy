package uk.co.nakedelement.k5buoy;

public class Metric
{
	private String key;
	private String value;
	private String unit;
	
	public String getKey()
	{
		return key;
	}
	
	public Metric setKey(String key)
	{
		this.key = key;
		return this;
	}

	public String getValue()
	{
		return value;
	}
	
	public Metric setValue(String value)
	{
		this.value = value;
		return this;
	}
	
	public String getUnit()
	{
		return unit;
	}
	
	public Metric setUnit(String unit)
	{
		this.unit = unit;
		return this;
	}
	
	@Override
	public String toString()
	{
		final StringBuilder builder = new StringBuilder();
		
		builder.append(getKey());
		builder.append(" ");
		builder.append(getValue());
		builder.append(" ");
		builder.append(getUnit());
		
		return builder.toString();
	}
}
