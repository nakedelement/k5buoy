package uk.co.nakedelement.k5buoy;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Map;
import java.util.Set;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class Results
{
	private final Map<Date, Map<String, Metric>> results;
	
	public Results(Map<Date, Map<String, Metric>> results)
	{
		this.results = results;
	}
	
	public boolean isEmpty()
	{
		return results.isEmpty();
	}
	
	public Set<Date> dates()
	{
		return results.keySet();
	}
	
	public Map<String, Metric> metricsBy(Date date)
	{
		return results.get(date);
	}
	
	public Collection<Collection<Metric>> byLine()
	{
		final Collection<Collection<Metric>> lines = new ArrayList<>();
		
		for(final Date date: dates())
		{
			final Map<String, Metric> metricsByLabel = metricsBy(date);
			final Collection<Metric> metrics = new ArrayList<>();
			
			for(final Metric metric : metricsByLabel.values())
				metrics.add(metric);
			
			lines.add(metrics);
		}
		
		return lines;
	}
	
	public String toJson()
	{
		try
	    {
			return new ObjectMapper().writeValueAsString(byLine());
	    }
	    catch (JsonProcessingException e)
	    {
	    	throw new K5BuoyException(e);
	    } 
	}
}
