package uk.co.nakedelement.k5buoy;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import java.util.Date;

import org.junit.Test;

public class K5BuoyTest
{
	@Test
	public void getResults()
	{
		final Results results = new K5Buoy().get();
		assertFalse(results.isEmpty());
		
		for(final Date date : results.dates())
			assertEquals(19, results.metricsBy(date).keySet().size());			
	}
	
	@Test(expected=K5BuoyException.class)
	public void unknownUrl()
	{
		new K5Buoy("http://nakedelement.co.uk/nothing/").get();					
	}
	
	@Test(expected=K5BuoyException.class)
	public void noData()
	{
		new K5Buoy("http://nakedelement.co.uk/").get();					
	}
	
	@Test(expected=K5BuoyException.class)
	public void malformedUrl()
	{
		new K5Buoy("").get();					
	}
	
	@Test
	public void resultsAsJson()
	{
		final String resultsAsJson = new K5Buoy().get().toJson();
		System.out.println(resultsAsJson);
	}

}
