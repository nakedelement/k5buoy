package uk.co.nakedelement.k5buoy;

import static org.junit.Assert.*;

import org.junit.Test;

public class MetricTest
{
	@Test
	public void toStringPrints()
	{
		assertEquals("key value unit", new Metric().setKey("key").setValue("value").setUnit("unit").toString());
	}

}
